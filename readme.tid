title: $:/plugins/OokTech/CaretPosition/readme

This lets you position a tiddler at the location of the caret in an active text
entry box.
It also includes the `$caret` widget described below.

Any contents of the tiddler [[$:/CaretContents]] are shown at the position of
the caret.

If you want to add another one make a tiddler and tag it with `Caret Marker`
and put a $caret widget in it and configure it how you like.
Note that the tiddler with the widget gets tagged, not the tidler with the
content you want displayed.

!!`$caret` Widget

|!Attribute |!Description |!Default |
|!tiddler |The tiddler that contains what you want displayed at the current cursor location |No Default |
|!persistent |If the tiddler contents should always remain visible even after the input loses focus. It can be set to `yes` or `no`. |`no` |

Example:

```
<$caret tiddler='MyTiddler' persistent='yes'/>
```


<!--

This gives the position of the caret (cursor in a text field) in the text field
that currently has focus.

Here `position` means two things:

* The index in the text field for the character where the caret currently is.
* The x,y coordinates of the caret on the screen in a form that you can use for css positioning

By default these values are stored in the tiddler `$:/state/CaretPosition`.

The caret index in the text field is stored in the field `position`, the x
coordinate of the caret on the screen is stored in the field `x` and the y
coordinate is stored in the field `y`.

Both the x and y values use px units.

!! Advanced

You can change the name of this tiddler by putting the name in the text field of
the tiddler $:/plugins/OokTech/CaretPosition/SaveTiddlerName
-->
The code is adapted from https://github.com/component/textarea-caret-position,
which is MIT licensed.