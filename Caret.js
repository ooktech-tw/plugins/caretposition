/*\
title: $:/plugins/OokTech/CaretPosition/caret.js
type: application/javascript
module-type: widget

Caret position widget

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var CaretWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
CaretWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
CaretWidget.prototype.render = function(parent,nextSibling) {
	var self = this;
	// Remember parent
	this.parentDomNode = parent;
	// Compute attributes and execute state
	this.computeAttributes();
	this.execute();
	// Create element
	var domNode = this.document.createElement("div");
	// Assign classes
	var classes = (this["class"] || "").split(" ");
	classes.push("tc-caret");
	//domNode.id = "cursor-label";
	domNode.style.left = $tw.caretX + 'px';
	domNode.style.top = $tw.caretY + 'px';
	domNode.className = classes.join(" ");
	domNode.style.display = ($tw.caretActive || this.persistent === 'yes') ? 'block' : 'none';
	// Insert element
	parent.insertBefore(domNode,nextSibling);
	this.renderChildren(domNode,null);
	this.domNodes.push(domNode);
};

/*
Compute the internal state of the widget
*/
CaretWidget.prototype.execute = function() {
	var self = this;
	// Get attributes
	this["class"] = this.getAttribute("class","");
	this.tiddler = this.getAttribute("tiddler", "");
	this.persistent = this.getAttribute("persistent", "no");
	this.active = false;
	this.lastX = 0;
	this.lastY = 0;
	var parser = this.wiki.parseTextReference(
						this.tiddler,
						undefined,
						undefined,
						{
							parseAsInline: false,
							subTiddler: false
						}),
		parseTreeNodes = parser ? parser.tree : this.parseTreeNode.children;
	// Set context variables for recursion detection
	var recursionMarker = this.makeRecursionMarker();
	this.setVariable("transclusion",recursionMarker);
	// Check for recursion
	if(parser) {
		if(this.parentWidget && this.parentWidget.hasVariable("transclusion",recursionMarker)) {
			parseTreeNodes = [{type: "element", tag: "span", attributes: {
				"class": {type: "string", value: "tc-error"}
			}, children: [
				{type: "text", text: $tw.language.getString("Error/RecursiveTransclusion")}
			]}];
		}
	}
	// Construct the child widgets
	this.makeChildWidgets(parseTreeNodes);
};

/*
Compose a string comprising the title, field and/or index to identify this transclusion for recursion detection
*/
CaretWidget.prototype.makeRecursionMarker = function() {
	var output = [];
	output.push("{");
	output.push(this.getVariable("currentTiddler",{defaultValue: ""}));
	output.push("|");
	output.push(this.transcludeTitle || "");
	output.push("|");
	output.push(this.transcludeField || "");
	output.push("|");
	output.push(this.transcludeIndex || "");
	output.push("|");
	output.push(this.transcludeSubTiddler || "");
	output.push("}");
	return output.join("");
};

/*
Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
*/
CaretWidget.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if(this.lastX !== $tw.caretX || this.lastY !== $tw.caretY || this.active !== $tw.caretActive) {
		this.active = $tw.caretActive;
		this.lastX = $tw.caretX;
		this.lastY = $tw.caretY;
		this.refreshSelf();
		return true
	}
	return this.refreshChildren(changedTiddlers);
};

exports.caret = CaretWidget;

})();
