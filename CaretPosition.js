/*\
title: $:/plugins/OokTech/CaretPosition/CaretPosition.js
type: application/javascript
module-type: startup

this records the caret position on every keyup event and stores both the x,y
coordinates and the index in the text field in the tiddler
CARET_POSITION_TIDDLER
\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  // Export name and synchronous status
  exports.name = "mouse-position";
  exports.platforms = ["browser"];
  exports.after = ["render"];
  exports.synchronous = true;

  if ($tw.browser) {
    let CARET_POSITION_TIDDLER = '$:/state/CaretPosition';
    // Taken from http://jsfiddle.net/dandv/aFPA7/
    // Original repo: https://github.com/component/textarea-caret-position

    // The properties that we copy into a mirrored div.
    // Note that some browsers, such as Firefox,
    // do not concatenate properties, i.e. padding-top, bottom etc. -> padding,
    // so we have to do every single property specifically.
    var properties = [
      'boxSizing',
      'width',  // on Chrome and IE, exclude the scrollbar, so the mirror div wraps exactly as the textarea does
      'height',
      'overflowX',
      'overflowY',  // copy the scrollbar for IE

      'borderTopWidth',
      'borderRightWidth',
      'borderBottomWidth',
      'borderLeftWidth',

      'paddingTop',
      'paddingRight',
      'paddingBottom',
      'paddingLeft',

      // https://developer.mozilla.org/en-US/docs/Web/CSS/font
      'fontStyle',
      'fontVariant',
      'fontWeight',
      'fontStretch',
      'fontSize',
      'lineHeight',
      'fontFamily',

      'textAlign',
      'textTransform',
      'textIndent',
      'textDecoration',  // might not make a difference, but better be safe

      'letterSpacing',
      'wordSpacing'
    ];

    var isFirefox = !(window.mozInnerScreenX == null);
    var mirrorDiv, computed, style;

    var getCaretCoordinates = function (element, position) {
      // mirrored div
      mirrorDiv = document.getElementById(element.nodeName + '--mirror-div');
      if (!mirrorDiv) {
        mirrorDiv = document.createElement('div');
        mirrorDiv.id = element.nodeName + '--mirror-div';
        document.body.appendChild(mirrorDiv);
      }

      style = mirrorDiv.style;
      computed = getComputedStyle(element);

      // default textarea styles
      style.whiteSpace = 'pre-wrap';
      if (element.nodeName !== 'INPUT')
        style.wordWrap = 'break-word';  // only for textarea-s

      // position off-screen
      style.position = 'absolute';  // required to return coordinates properly
      style.top = element.offsetTop + parseInt(computed.borderTopWidth) + 'px';
      style.left = "400px";
      style.visibility = 'hidden';  // not 'display: none' because we want rendering

      // transfer the element's properties to the div
      properties.forEach(function (prop) {
        style[prop] = computed[prop];
      });

      if (isFirefox) {
        style.width = parseInt(computed.width) - 2 + 'px'  // Firefox adds 2 pixels to the padding - https://bugzilla.mozilla.org/show_bug.cgi?id=753662
        // Firefox lies about the overflow property for textareas: https://bugzilla.mozilla.org/show_bug.cgi?id=984275
        if (element.scrollHeight > parseInt(computed.height))
          style.overflowY = 'scroll';
      } else {
        style.overflow = 'hidden';  // for Chrome to not render a scrollbar; IE keeps overflowY = 'scroll'
      }

      mirrorDiv.textContent = element.value.substring(0, position);
      // the second special handling for input type="text" vs textarea: spaces need to be replaced with non-breaking spaces - http://stackoverflow.com/a/13402035/1269037
      if (element.nodeName === 'INPUT')
        mirrorDiv.textContent = mirrorDiv.textContent.replace(/\s/g, "\u00a0");

      var span = document.createElement('span');
      // Wrapping must be replicated *exactly*, including when a long word gets
      // onto the next line, with whitespace at the end of the line before (#7).
      // The  *only* reliable way to do that is to copy the *entire* rest of the
      // textarea's content into the <span> created at the caret position.
      // for inputs, just '.' would be enough, but why bother?
      span.textContent = element.value.substring(position) || '.';  // || because a completely empty faux span doesn't render at all
      span.style.backgroundColor = "lightgrey";
      mirrorDiv.appendChild(span);

      var coordinates = {
        top: span.offsetTop + parseInt(computed['borderTopWidth']),
        left: span.offsetLeft + parseInt(computed['borderLeftWidth'])
      };

      return coordinates;
    }

    // Adapted from https://stackoverflow.com/questions/4928586/get-caret-position-in-html-input
    function getInputSelection(el) {
      var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

      if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
      } else {
        range = document.selection.createRange();
        if (range && range.parentElement() == el) {
          len = el.value.length;
          normalizedValue = el.value.replace(/\r\n/g, "\n");
          // Create a working TextRange that lives only in the input
          textInputRange = el.createTextRange();
          textInputRange.moveToBookmark(range.getBookmark());

          // Check if the start and end of the selection are at the very end
          // of the input, since moveStart/moveEnd doesn't return what we want
          // in those cases
          endRange = el.createTextRange();
          endRange.collapse(false);
          if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
            start = end = len;
          } else {
            start = -textInputRange.moveStart("character", -len);
            start += normalizedValue.slice(0, start).split("\n").length - 1;
            if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
              end = len;
            } else {
              end = -textInputRange.moveEnd("character", -len);
              end += normalizedValue.slice(0, end).split("\n").length - 1;
            }
          }
        }
      }
      return {
          start: start,
          end: end
      };
    }
    document.addEventListener("keyup", function(e) {
      const domNode = document.activeElement;
      if(domNode) {
        try {
					/*
          $tw.popup.triggerPopup({
						domNode: domNode,
						title: 'test',//this.widget.editFocusPopup,
						wiki: $tw.wiki,
						force: true
					});
					*/
          const position = getInputSelection(domNode);
          const realPosition = Math.max(position.end, position.start);
          const coord1 = getCaretCoordinates(domNode, realPosition);
          const coord2 = domNode.getBoundingClientRect();
          const newCoords = {x: Math.round(coord1.left + coord2.x), y: Math.round(coord1.top + coord2.y + window.scrollY)};
          if($tw.caretX !== newCoords.x || $tw.caretY !== newCoords.y) {
            //const saveName = $tw.wiki.getTiddlerText('$:/plugins/OokTech/CaretPosition/SaveTiddlerName') || CARET_POSITION_TIDDLER;
            $tw.caretX = newCoords.x;
            $tw.caretY = newCoords.y;
            $tw.caretActive = true;
          }
          domNode.addEventListener('blur', function(e) {
            $tw.caretActive = false;
          })
        } catch {
          // Nothing here
        }
      }
    })
  }
})();
